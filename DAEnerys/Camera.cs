﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DAEnerys
{
    public class Camera
    {
        public Vector3 Position = new Vector3(0, 0, 15);
        public Vector3 Orientation = new Vector3((float)Math.PI, 0f, 0f);
        private Matrix4 viewMatrix = Matrix4.Identity();

        public float NearClipDistance = 0.01f;
        public float ClipDistance = 1000;
        public float FieldOfView = 1.22f;
        

        private bool orthographic;
        public bool Orthographic { get { return orthographic; } set { orthographic = value; Update(true); } }

        private float orthographicSize = 12;
        public float OrthographicSize { get { return orthographicSize; } set { orthographicSize = value; Update(); } }
        private float perspectiveZoom = 1;

        private float lastOrthographicSize;

        public float CalculatedZoom = 1;
        public float ZoomSpeed = 5;

        private Vector3 orbitPoint = Vector3.Zero;

        private float zoom = 1;
        public float Zoom { get { return zoom; } set { zoom = value; Update(); } }
        

        private float lastZoom;
        private Vector2 angles = new Vector2((float)Math.PI, (float)Math.PI);

        private float lastWheelPrecise;

        private Point lastPos;
        private Point pressPos;

        //private Cursor cursor = new Cursor(Program.main.Cursor.Handle);

        public void MouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MouseState mouse = Mouse.GetState();
                Cursor.Hide();
                pressPos = Cursor.Position;
            }
        }

        public void MouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //Vector2 newPos = new Vector2(Program.GLControl.Bounds.Left + pressPos.X, Program.GLControl.Bounds.Top + pressPos.Y);
                //Mouse.SetPosition(newPos.X, newPos.Y);
                Cursor.Position = pressPos;
                Cursor.Show();
                pressPos = Point.Empty;
            }
        }

        public void MouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            bool leftButton = (e.Button == MouseButtons.Left);
            bool rightButton = (e.Button == MouseButtons.Right);
            Point delta = Cursor.Position - lastPos;
            
            if (!leftButton && rightButton)
                Rotate(delta);
            else if (leftButton && !rightButton)
                Pan(delta);
            else if (leftButton && rightButton)
                Zoom(delta.Y);
        }

        public void MouseWheel(System.Windows.Forms.MouseEventArgs e)
        {
            //Not sure if this works just yet...
            if (e.Delta != 0)
                Zoom(e.Delta);
        }

        public void KeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (ActionKey.IsDown(Action.TOGGLE_ORTHOGRAPHIC)) //Toggle orthographic view
            {
                this.Orthographic = !this.Orthographic;
                if (this.Orthographic)
                {
                    this.perspectiveZoom = this.Zoom;
                    this.Zoom = CalculatedZoom;
                }
                else
                    this.Zoom = perspectiveZoom;

                Program.main.UpdatePerspectiveOrthoCombo();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_FRONT)) 
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)Math.PI;

                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_BACK))
            {
                angles.X = (float)Math.PI;
                angles.Y = 0;

                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_LEFT)) 
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)-Math.PI / 2;

                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_RIGHT))
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)Math.PI / 2;

                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_TOP)) 
            {
                angles.X = (float)Math.PI * 1.5f;
                angles.Y = (float)Math.PI;

                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_BOTTOM))
            {
                angles.X = 0;
                angles.Y = (float)Math.PI;
                
                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
        }
        
        private void Rotate(Point delta) {
            angles.X += delta.Y * 0.01f;
            angles.Y -= delta.X * 0.01f;
            
            UpdatePosition();
        }
        
        private void Zoom(float delta) {
            if (!this.Orthographic)
                zoom -= zoomDelta * ZoomSpeed * 0.01f;
            else
                orthographicSize += zoomDelta * (orthographicSize / 30);
            
            UpdatePosition();
        }
        
        private void Pan(Point delta) {
            
            // UpdatePosition();
        }
        
        private void ConstrainValues() {
            angles.X = (float)Utilities.Clamp(angles.X, Math.PI * 0.5, (Math.PI * 1.5) - 0.000001f);
            angles.Y = (float)(angles.Y % (2.0 * Math.PI));
            zoom = Utilities.Clamp(zoom, 0.001f, 500000);
        }
        
        public void Update(bool forceUpdate = false)
        {
            if (Program.GLControl == null)
                return;

            if (Program.GLControl.Focused || forceUpdate)
            {
                UpdatePosition();
                Renderer.UpdateView();
                Program.GLControl.Invalidate();
            }
        }

        private void UpdatePosition()
        {
            ConstrainValues();
            Position = orbitPoint + Vector3.Transform(new Vector3(0, 0, Zoom), Matrix4.CreateRotationX(angles.X) * Matrix4.CreateRotationY(angles.Y));
            viewMatrix = Matrix4.LookAt(Position, orbitPoint, new Vector3(0, 1, 0));
        }

        public Matrix4 GetViewMatrix()
        {
            return viewMatrix;
        }
    }
}
